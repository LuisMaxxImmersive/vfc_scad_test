include <E:\Users\luisr\OneDrive\Desktop\SCAD_Modules\modules.scad>

//  #VARS
$fn=90;
rodR = 3.98 / 2;
R =1.3;
	
	// global parameters
	

module camera(  ) {
	thick = 1;
	W =32;
	L = 32;
	H = 1.2;
	CW =6;
	CL = 12;
	CH = 18;
	CR = 4;
	rotate( [180 , 0 , 0 ] )
	difference(){
	union(){
		
		box( W +1, L+1 , H+1 ) ;
		cylinder( r = 6, h = CH , center=true);
		translate( [ L/2-CL/2-7 ,W/2-CW/2-1.2 , 0 ] )box( CL,  CW,  CH ) ;
		translate( [ (16-4.75), (16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
		translate( [ (16-4.75), -(16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
		translate( [ -(16-4.75), (16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
		translate( [ -(16-4.75), -(16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
	} # union() {
		}  }
	}
module strip_camera( type = 1 ) {
	thick = 1;
	W = 18;
	L = W;
	H = 5;
	difference(){
	union(){
		box( W, L , H ) ;
	} # union() {
		}  }
	}


module camera_case( type = 2 ) {
	thick = 1;
	W = 35;
	L = W;
	H = 2;
	H2 =6;
	M= 3;
	difference(){
	union(){
		difference(){
		union(){
			box( W, L , H2 ) ;
		} #union(){
			translate( [ 0 , 0 , H] )  box( W-1, L-1 , H2-H ) ;
			} }
		difference(){
		union(){
			box( W, L , H ) ;
			translate( [ -26.5, 0 , 0 ] )  box( 20, 6 , 6 ) ;
			difference(){
			union(){
				translate( [ (16-4.75), (16-4.75) , H ] )  cylinder( r = 3, h = M, center= false);
				translate( [ (16-4.75), -(16-4.75) , H ] )  cylinder( r = 3, h = M, center= false);
				translate( [ -(16-4.75), (16-4.75) , H ] )  cylinder( r = 2.5, h = M, center= false);
				translate( [ -(16-4.75), -(16-4.75) , H ] )  cylinder( r = 3, h = M, center= false);
			} #union(){
				translate( [ (16-4.75), (16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
				translate( [ (16-4.75), -(16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
				translate( [ -(16-4.75), (16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
				translate( [ -(16-4.75), -(16-4.75) , 0 ] )  cylinder( r = R, h = 10, center= true);
				} }
		} # union() {
			translate( [ -27 , 0 ,3] )  rotate( [ 0 , 90 , 0 ] )   cylinder( r = rodR , h = 20, center= true);
			}  }
	} #union(){
		translate( [ 0 , 0 , 6 ] )  rotate( [ 0 ,  0 , 90 ] )  camera() ;
		} }
	}
hex_block() ;
module hex_block( ) {
	W = 12;
	L = W ;
	difference(){
	union(){
		cylinder( r = W, h = 20);
		translate( [ -13 , 4.2 ,  ] )  box( 6, 3 , 20 ) ;
		translate( [ -13 , -4.2 ,  ] )  box( 6, 3 , 20 ) ;
	} # union() {
		translate( [ -14 , 0 , 0 ] )  box( 6, 6 , 20 ) ;
		translate( [ 0 , 0 , 10] )  rotate( [ 0 , 90 , 0 ] )   cylinder( r = rodR , h = 50, center= true);
		for  ( t = [ 0:120:300 ]) {
			translate( [ L*sin(t)/1.5, W*cos(t)/1.5, 0 ] )  cylinder( r = rodR ,  h = 50, center= true);
			}
		
		}  }
	}



module stand( ) {
	W =30;
	L = W ;
	difference(){
	union(){
		cylinder( r = W, h = 3);
		cylinder( r = 4, h = 20);
	} # union() {
		translate( [ 0 , 0 , 2] )  cylinder( r = rodR, h = 50, center= false);
		}  }
	}

module rig( ) {
	thick = 1;
	W = 20;
	L = 26;
	
	translate( [ -30, -30,  0 ] )  lock_case() ;
	difference(){
	union(){
		translate( [ 0 , -44, 0 ] )  camera_case() ;
		box(L, W+2, H ) ;
	} # union() {
		box( 10, 25 , 3 ) ;
		translate( [ 0 , 2, 0 ] )  box( 18.75 , W+1, H ) ;
		translate( [-15, -4 , H/2 ] )  rotate( [  0 , 90 , 0] )   cylinder( r = 1.2, h = 40);
		translate( [-15, 5 , H/2 ] )  rotate( [  0 , 90 , 0] )   cylinder( r = 1.2, h = 40);
		}  }
	}

module multi_rig( ) {
	for  (A= [ 0:88:300 ]) {
		translate( [ 0 , A, 0 ] )  rig() ;
		}
	
	}

module ShelfPlate( ) {
	L = 35 ;
	W = 100 ;
	H = 10;
	D = 21;
	thick = 1;
	T =[ 0 , 60 ,  W/2 ];
	R = [ 35 , 0 , 0 ] ;
	difference(){
	union(){
		box( W , L , H  ) ;
		}
	# union() {
		translate( [ 65/2 , 0, 0 ] )  cylinder( r = 1.7, h = 25);
		translate( [-65/2 , 0, 0 ] )  cylinder( r = 1.7, h = 25);
		translate( [ 0 , 5, 0 ] )  box( 92,  30 , 3 );
		translate( [ 0 , 0, 0 ] ) cylinder( r=28/2 , h = 30, center = true );
		translate( [ 0 , 15, 0 ] ) box( 28 , 28, 30 );
		}
		}
	}

module  cellBank( ) {
	T= 5;
	dT = .2 ;
	difference(){
	union(){
		
		translate( [ Dia*2.5+2, 0, 0 ] )
		box( 6, 26 , T+6  );
		rounded_box( [ (5*Dia+2*thick)  , 2*(Dia+thick) +dT  , 10], 3  );
	} # union(){
		box( 80 , 18  , 30 );
		translate( [ Dia*2.5+2, (Dia+1), 0 ] )
		rotate( [ 0 ,  0 , 45 ] )
		box(8 ,8  , 30 );
		translate( [ Dia*2.5+2, -(Dia+1), 0 ] )
		rotate( [ 0 ,  0 , 45 ] )
		box(8 ,8  , 30 );
		translate( [ Dia*2.5+2, 0, 0 ] )
		rotate( [ 0 ,  0 , 45 ] )
		box(8 ,8  , 30 );
		translate( [ -(Dia*2.5+1), 0, 0 ] )
		rotate( [ 0 ,  0 , 45 ] )
		box(8 ,8  , 30 );
		for  ( X= [ -5*R : Dia : 4*R ]) {
			translate( [ X+R+thick, -(Dia + dT)/2 , -.5 ] )
			cylinder( r = R, h = height+20 );
			translate( [ X+R+thick,  (Dia +dT)/2 , -.5 ] )
			cylinder( r = R, h = height+20 );
			}
		
		}  }
	}
module  cellBank1( ) {
	T= 5;
	dT = .4 ;
	difference(){
	union(){
		color("blue")
		translate( [ 96, 13-R/2+thick+dT , 0 ] )
		box( 5.5 , 26 , T+6  );
		translate( [ 0 , Dia + thick+dT, 0 ] )
		cellStrip();
		cellStrip();
		
		
	} # union(){
		translate( [ 95, 13.5-R/2+thick+dT, 0 ] )
		rotate( [ 0 ,  0 , 45 ] )
		box(8 ,8  , 30 );
		for  ( X= [ 0:Dia:9*R ]) {
			translate( [ X+R+thick, 0 , -.5 ] )
			cylinder( r = R, h = height+20 );
			}
		
		for  ( X= [ 0:Dia:9*R ]) {
			translate( [ X+R+thick, Dia + thick+dT , -.5 ] )
			cylinder( r = R, h = height+20 );
			}
		
		}  }
	}

module  cellStrip( ) {
	dT=1;
	difference(){
	union(){
		
		for  ( X= [ 0:Dia:9*R ]) {
			translate( [ X+R+thick, 0 , 0 ] )
			ring( R , thick , height ) ;
			}
		
		
		translate( [ 47 , R-4, 0 ] )  box( 75 , 8 , 10 );
		translate( [ 47 , -R+4, 0 ] )  box( 75, 8 , 10 );
		
	} union(){
		for  ( X= [ 0:Dia:9*R ]) {
			translate( [ X+R+thick, 0 , -.5 ] )
			cylinder( r = R, h = height+20 );
			}
		
		}  }
	}

module  cellCap( ) {
	T= 5;
	difference(){
	union(){
		ring( R , thick , height ) ;
		
	} # union(){
		}  }
	}

module  packEnd( ) {
	L= 101.5;
	W = 42.5;
	H = 13.0;
	thick = 2.35;
	flange_thick = 1;
	difference(){
	union(){
		rounded_box( [ W,  L , H - thick] , 3 );
		rounded_box( [ W-flange_thick*2,  L-flange_thick*2 , H] , 3 );
	} # union(){
		translate( [ 0 , 0 , thick ] )
		rounded_box( [ W-flange_thick*4,  L-flange_thick*4 , H] , 3 );
		translate( [ W/2-4, L/2-4, 0 ] )
		{ translate( [0,0,H-thick]) cylinder( r = 4, h = 5);  cylinder( r = 1.5 , h = 20 ); }
		translate( [ -(W/2-4), L/2-4, 0 ] )
		{ translate( [0,0,H-thick]) cylinder( r = 4, h = 5);  cylinder( r = 1.5 , h = 20 ); }
		translate( [ 0, -(L/2-4), 0 ] )
		{ translate( [0,0,H-thick]) cylinder( r = 4, h = 5);  cylinder( r = 1.5 , h = 20 ); }
		}  }
	}

module  multiCharger( ) {
	T= 75;
	difference(){
	union(){
		for  ( X = [ 1: 1: 2 ]) {
			translate( [ 4 , X*( Dia + 6),  0 ] )
			cellHolder( ) ;
			}
		
		translate( [-R-2,T/2+15, 0 ] )
		box( 3 , 3*Dia + 25 , T +8);
	} # union(){
		
		
		}  }
	}

module  cellHolder( ) {
	T= 75;
	thick = 5;
	difference(){
	union(){
		ring( R , thick , T+6 ) ;
		cylinder( r = R , h = 3 ) ;
		translate( [ 0 , 0 ,T + 3 ] )
		cylinder( r = R , h = 3 ) ;
	} union(){
		translate( [ 0, 0, T ] )
		box( 30 , 10  , 2 ) ;
		
		translate( [ 0, 0, 3 ] )
		box( 30 , 10  , 2 ) ;
		
		translate( [ R+6, 0, 3 ] )
		box( Dia+13, Dia+13 , T ) ;
		cylinder( r = 1.6 , h = T*2 ) ;
		}  }
	}

module  cellTester( ) {
	T= 75;
	difference(){
	union(){
		ring( R , thick , T+6 ) ;
		cylinder( r = R , h = 3 ) ;
		translate( [ 0 , 0 ,T + 3 ] )
		cylinder( r = R , h = 3 ) ;
		translate( [-R-1,T/2, 0 ] )
		box( 3 , T+20 , T + 4);
	} union(){
		translate( [ 0, 0, T ] )
		box( 30 , 10  , 2 ) ;
		
		translate( [ 0, 0, 3 ] )
		box( 30 , 10  , 2 ) ;
		
		translate( [ R/2, 0, 3 ] )
		box( Dia , Dia+3 , T ) ;
		cylinder( r = 1.6 , h = T*2 ) ;
		}  }
	}

module  cellBalancer( ) {
	L= 101.5;
	W = 42.5;
	H = 3.0;
	T= 5;
	thick = 2.35;
	for  ( X= [ 0:Dia:9*R ]) {
		translate( [ X+R+thick, R , -.5 + T ] )
		cylinder( r = 4 , h =2 ) ;
		translate( [ X+R+thick, -R , -.5 + T ] )
		cylinder( r = 4 , h = 2) ;
		}
	
	difference(){
	union(){
		translate( [L/2 ,0, -1  ] )
		box( L,W,H +T  );
	} union(){
		for  ( X= [ 0:Dia:9*R ]) {
			translate( [ X+R+thick, R , -.5 + T ] )
			cylinder( r = R+thick, h = T );
			translate( [ X+R+thick, -R , -.5 + T ] )
			cylinder( r = R+thick, h = T );
			}
		
		translate( [ 95, 0  , T ] )
		box( 5.5 , 26 , T+2  );
		}  }
	}

module  cam( ) {
	difference(){
	union(){
		mcad_poly( 8 );
	}union(){
		cylinder( r = .8, h = 35, center=true );
		translate( [ 0 , 0 , 4.6 ] )  cylinder( r = 4.8, h =35);
		cylinder( r = 2.7 , h =3.6 );
		} }
	}

module  servoRelay( ) {
	difference(){
	union(){
		Push = 5.5;
		translate( [ 7 , 15+Push  , 28 ] )
		rotate( [ 90, 0, 0 ])
		button_hold( ) ;
		%translate( [ 6 , 0 , 28] )  rotate([0,0,90])mcad_poly( 8 );
		translate( [ 0 ,1.2 , 1] )  box( 40, 15, 18 );
	} # union(){
		translate( [ 0 , 0 , 1 ] )  servo_sg90();
		translate( [ -18 , 0 ,15 ] )  rotate( [ 90 , 0 , 0 ] )  cylinder( r = .8, h = 35, center=true );
		translate( [ 18 , 0 ,15 ] )  rotate( [ 90 , 0 , 0 ] )  cylinder( r = .8, h = 35, center=true );
		}  }
	}

module  button_hold( ) {
	difference(){
	union(){
		box( 20 , 30 , 3  );
		translate( [ 00, -12.5 , 0 ] )
		box( 30 , 7 , 20  );
		translate( [ 12, 0 , 0 ] )  box( 5, 30, 8 );
		translate( [ -12, 0 , 0 ] )  box( 5, 30, 8 );
	}union(){
		translate( [ 0 , 4, 0 ] )
		cylinder( r = 13/2, h = 5);
		} }
	}
//---------------------------------------------------------------------------------------------------------------------------
	
module  pickFingers( ) {
	S = 15;
	difference(){
	union(){
		translate( [ 0 , 0 , 3] ) pickFinger();
		
	} # union(){
		}  }
	}

module  pickFinger( ) {
	difference(){
	union(){
		%translate( [ 6 , 0 , 28] )  rotate([0,0,180])mcad_poly( 4 );
		translate( [ 0 ,1.2 , 3] )  box( 40, 15, 15 );
	} # union(){
		translate( [ 0 , 0 , 0 ] )  servo_sg90();
		translate( [ -18 , 0 ,15 ] )  rotate( [ 90 , 0 , 0 ] )  cylinder( r = .8, h = 35, center=true );
		translate( [ 18 , 0 ,15 ] )  rotate( [ 90 , 0 , 0 ] )  cylinder( r = .8, h = 35, center=true );
		}  }
	}

module  pick( ) {
	difference(){
	union(){
		rotate([0,0,180])mcad_poly( 4 );
	} # union(){
		translate( [ 0 , 0 , 1.5  ] )  cylinder( r = 2.2, h = 3 );
		cylinder( r = 1.2, h = 3 );
		}}
	}
module  neckBrace() {
	difference(){
	union(){
		box( dx+4,dz+4, 11);
	} union(){
		box( dx+.3, dz+.3, 11 );
		}  }
	}
module  wheel( ) {
	difference(){
	union() {
		rotate( [ 90 , 0 , 0 ] )  cylinder( r = 13-.2,  h = Cal*2.2-.1, center=true );
	} # union(){
		rotate( [ 90 , 0 , 0 ] )  cylinder( r = 1,  h = 15, center=true );
		for  (A= [ 0:90:300 ]) {
			rotate( [ 0 , A , 0 ] )   translate( [ 10,0,0 ] ) catch( ) ;
			}
		
		}
		}
	
	}
module  catch( ) {
	translate( [ 0 , 0 , 0 ] )
	difference(){
	union(){
		sphere( r = Cal );
		
		cylinder( r = Cal , h = 10 );
		rotate( [ 0 , 100 , 0 ] )  cylinder( r = Cal , h = 10 );
		rotate( [ 0 , 45 , 0 ] )  cylinder( r = Cal , h = 10 );
	} union(){
		}
	}
	}

module  servos( ) {
	rotate( [ -90, 0, 0 ])
	servo() ;
	translate( [ 0 , 45 , 0 ] )
	rotate( [ -90, 0, -90 ])
	servo() ;
	translate( [ 30 , 40 , 0 ] )
	rotate( [ -90, 130, -90 ])
	servo() ;
	}
//---------------------------------------------------------------------------------------------------------------------------
	
module  screwshaft(  T, R, rad=1.56, len = 150 ) {
	
	translate( T )
	rotate( R )
	cylinder( r = rad, h = len, center = true );
	}

module  countersink(  T = [ 0,0,3 ], R, rad=3, len = 6) {
	
	translate( T )
	rotate( R )
	cylinder( r = rad, h = len, center = true );
	}
//  Export  Date: 10:06:46 PM - 29:Sep:2022...

